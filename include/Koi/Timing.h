#pragma once

#include <GLFW/glfw3.h>

namespace Koi
{
	class FPSLimiter
	{
	public:
		FPSLimiter()
		{

		}

		void init(float limit);
		void start();
		void setLimit(float limit);

		// Returns FPS
		float limit();

	private:
		void calculateFPS();

		float m_limit;
		float m_frameTime;
		float m_FPS;
		unsigned int m_startTicks;
	};
}
