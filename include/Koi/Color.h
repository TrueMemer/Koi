#pragma once

#include <GL/glew.h>

namespace Koi {

    struct ColorRGBA8 {
        ColorRGBA8() : r(0), g(0), b(0), a(0) {}
        ColorRGBA8(GLubyte _r, GLubyte _g, GLubyte _b, GLubyte _a) : r(_r), g(_g), b(_b), a(_a) {}
        GLubyte r;
        GLubyte g;
        GLubyte b;
        GLubyte a;
    };

}
