#pragma once

#include <Koi/GLTexture.h>

#include <string>

namespace Koi
{

	class ImageLoader
	{
	public:
		static GLTexture loadPNG(std::string path);
	};

}
