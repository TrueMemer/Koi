#pragma once

#include <vector>
#include <string>

namespace Koi
{

	class IOManager
	{
	public:
		static bool readFileToBuffer(std::string& path, std::vector<unsigned char>& buf);
	};

}

