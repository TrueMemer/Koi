#pragma once

#include <glm/glm.hpp>

#include <unordered_map>

using namespace glm;

namespace Koi {

	class InputManager

	{
	public:
		InputManager();
		~InputManager();

		void keypress(unsigned int key);
		void release(unsigned int key);

		void update();

		void setMouseCoords(float x, float y);

		bool isKeyDown(unsigned int key);
		bool isKeyPressed(unsigned int key);
		vec2 getMouseCoords() const { return m_mouseCoords; }

	private:

		bool wasKeyDown(unsigned int key);

		std::unordered_map<unsigned int, bool> m_keymap;
		std::unordered_map<unsigned int, bool> m_lastKeymap;
		vec2 m_mouseCoords;
	};

}
