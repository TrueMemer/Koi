#pragma once

#include <GL/glew.h>

namespace Koi
{
	struct GLTexture {
		GLuint id;
		int width;
		int height;
	};
}