#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <string>

#include "Vertex.h"

enum WindowFlags
{
	INVISIBLE = 0x1,
	FULLSCREEN = 0x2,
	BORDERLESS = 0x4
};

namespace Koi
{

	class Window
	{
	public:
		Window();
		~Window();

		int create(std::string title, int width, int height, unsigned int flags);

		void update();
		void setBackgroundColor(ColorRGBA8 color);

		int getWidth() { return m_width; }
		int getHeight() { return m_height; }

		GLFWwindow *getGLFWWindow();

	private:
		GLFWwindow *m_window;

		int m_width, m_height;

		double frameTime, lastFrameTime;
		ColorRGBA8 m_backgroundColor = { 1, 1, 1, 1 };
	};

}
