#pragma once

#include "TextureCache.h"

namespace Koi
{

	class ResourceManager
	{
	public:
		static GLTexture getTexture(std::string path);

	private:
		static TextureCache m_textureCache;
	};

}

