#pragma once

#include <map>
#include <string>

#include "GLTexture.h"

namespace Koi
{

	class TextureCache
	{
	public:
		TextureCache();
		~TextureCache();

		GLTexture getTexture(std::string path);

	private:
		std::map<std::string, GLTexture> m_cache;
	};

}

