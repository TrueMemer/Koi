#pragma once

#include <string>

#include <GL/glew.h>

namespace Koi
{
	class GLSL
	{
	public:
		GLSL();
		~GLSL();

		void compileShaders(const std::string& vertex, const std::string& frag);
		void linkShaders();
		void addAttribute(const std::string& attrib);

		GLint getUniformLocation(const std::string& uniform);

		void use();
		void unuse();

	private:
		int attr = 0;

		GLuint m_id = 0;

		GLuint m_vertexID = 0;
		GLuint m_fragmentID = 0;

		void compile(const std::string& path, GLuint id);
	};
}

