#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <vector>

#include "Vertex.h"

using namespace glm;

namespace Koi {

	enum class GlyphSortType 
	{
		NONE,
		FRONT_TO_BACK,
		BACK_TO_FRONT,
		TEXTURE
	};

	class Glyph 
	{
	public:
		Glyph();
		Glyph(const vec4& rect, const vec4& UVRect, GLuint _texture, const ColorRGBA8& color, float _depth);
		Glyph(const vec4& rect, const vec4& UVRect, GLuint _texture, const ColorRGBA8& color, float _depth, float angle);
		GLuint texture;
		float depth;

		Vertex topLeft;
		Vertex bottomLeft;
		Vertex topRight;
		Vertex bottomRight;
	private:
		vec2 rotate(vec2 pos, float angle);
	};

	class Batch
	{
	public:

		Batch(GLuint _offset, GLuint _numVertices, GLuint _texture) :
			offset(_offset), numVertices(_numVertices), texture(_texture)
		{

		}

		GLuint offset;
		GLuint numVertices;
		GLuint texture;
	};

	class SpriteBatch
	{
	public:
		SpriteBatch();
		~SpriteBatch();

		void init();

		void begin(GlyphSortType sortType = GlyphSortType::TEXTURE);
		void end();

		void draw(const vec4& rect, const vec4& UVRect, GLuint texture, const ColorRGBA8& color, float depth);
		void draw(const vec4& rect, const vec4& UVRect, GLuint texture, const ColorRGBA8& color, float depth, float angle);
		void draw(const vec4& rect, const vec4& UVRect, GLuint texture, const ColorRGBA8& color, float depth, const vec2 dir);

		void renderBatch();

	private:
		void createRenderBatches();
		void createVertexArray();
		void sortGlyphs();

		static bool compareFrontToBack(Glyph *a, Glyph *b);
		static bool compareBackToFront(Glyph *a, Glyph *b);
		static bool compareTexture(Glyph *a, Glyph *b);

		GLuint m_vbo = 0;
		GLuint m_vao = 0;

		GlyphSortType m_sortType;

		std::vector<Glyph*> m_glyphPtrs;
		std::vector<Glyph> m_glyphs;
		std::vector<Batch> m_batches;

	};

}

