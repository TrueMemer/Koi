#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace glm;

namespace Koi
{

	class Camera2D
	{
	public:
		Camera2D();
		~Camera2D();

		void init(int width, int height);
		void update();

		bool isVisible(const vec2 &position, const vec2 &dimensions);

		vec2 getRealCoords(vec2 coords);

		// Setters
		void setPosition(const vec2& pos) { m_position = pos; m_needsUpdate = true; }
		void setScale(float s) { m_scale = s; m_needsUpdate = true; }

		// Getters
		vec2 getPosition() { return m_position; }
		float getScale() { return m_scale; }
		mat4 getCameraMatrix() { return m_camMatrix; }
	private:
		int m_screenWidth = 800, m_screenHeight = 600;
		bool m_needsUpdate = true;
		float m_scale = 1.f;
		vec2 m_position = vec2(0.f, 0.f);
		mat4 m_camMatrix = mat4(1.f);
		mat4 m_orthoMatrix = mat4(1.f);
	};

}

