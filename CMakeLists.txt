cmake_minimum_required(VERSION 3.8)

project(koi)
set(LIBRARY_NAME koi)
set(LIBRARY_FOLDER Koi)
include_directories("${CMAKE_SOURCE_DIR}/include")
file(GLOB SOURCE_LIB "${CMAKE_SOURCE_DIR}/src/*.cpp")
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${koi_SOURCE_DIR}/cmake/")
add_definitions(-DGLEW_STATIC)

if(MSVC)
        add_subdirectory(deps)
        include_directories(${glew_INCLUDE_DIR})
        include_directories(${glm_INCLUDE_DIR})
        include_directories(${glfw3_INCLUDE_DIR})
        ###
        link_directories(${glew_LIBRARY_DIR})
        link_directories(${glfw3_LIBRARY_DIR})
else()
        find_package(glm REQUIRED)
        if(NOT glm_FOUND)
                message(SEND_ERROR "Failed to find GLM")
                return()
        else()
                include_directories(${glm_INCLUDE_DIR})
        endif()
        ###
        find_package(glfw3 REQUIRED)
        if(NOT glfw3_FOUND)
                message(SEND_ERROR "Failed to find GLFW")
                return()
        else()
                include_directories(${glfw3_INCLUDE_DIR})
        endif()
endif()

add_library(koi STATIC ${SOURCE_LIB})
add_subdirectory(examples)
