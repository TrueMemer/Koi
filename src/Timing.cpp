#include <Koi/Timing.h>

#include <chrono>
#include <thread>

namespace Koi
{

	void FPSLimiter::init(float limit)
	{
		setLimit(limit);
	}

	void FPSLimiter::start()
	{
		m_startTicks = glfwGetTime();
	}

	float FPSLimiter::limit()
	{
		calculateFPS();

		// Time of one frame
		float ticks = glfwGetTime() - m_startTicks;

		// Limit FPS
		if (1000.f / m_limit > ticks)
		{
			//SDL_Delay(1000.f / m_limit - ticks);
			std::this_thread::sleep_for(std::chrono::duration<float, std::milli>(1000.f / m_limit - ticks));
		}

		return m_FPS;
	}

	void FPSLimiter::setLimit(float limit) {
		m_limit = limit;
	}

	void FPSLimiter::calculateFPS()
	{
		static const int SAMPLES = 10;
		static float frameTimes[SAMPLES];
		static int currFrame = 0;

		static float prevTicks = glfwGetTime();

		float currTicks;

		currTicks = glfwGetTime();

		m_frameTime = currTicks - prevTicks;
		frameTimes[currFrame % SAMPLES] = m_frameTime;

		prevTicks = currTicks;

		currFrame++;

		int count;
		if (currFrame < SAMPLES)
		{
			count = currFrame;
		}
		else
		{
			count = SAMPLES;
		}

		float avg = 0;

		for (int i = 0; i < count; i++)
		{
			avg += frameTimes[i];
		}
		avg /= count;

		if (avg > 0)
		{
			m_FPS = 1000.f / avg;
		}
		else {
			m_FPS = 60.f;
		}
	}
}
