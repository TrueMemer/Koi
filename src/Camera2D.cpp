#include <Koi/Camera2D.h>

namespace Koi
{

	Camera2D::Camera2D()
	{
	}


	Camera2D::~Camera2D()
	{
	}

	void Camera2D::init(int width, int height)
	{
		m_screenWidth = width;
		m_screenHeight = height;
		m_orthoMatrix = ortho(0.f, (float)m_screenWidth, 0.f, (float)m_screenHeight);
	}

	vec2 Camera2D::getRealCoords(vec2 coords)
	{
		coords -= vec2(m_screenWidth / 2, m_screenHeight / 2);
		coords /= m_scale;
		coords += m_position;
		return coords;
	}

	bool Camera2D::isVisible(const vec2 &position, const vec2 &dimensions)
	{
		vec2 scaledScreenDimensions = vec2(m_screenWidth, m_screenHeight) / (m_scale);

		// The minimum distance before a collision occurs
		const float MIN_DISTANCE_X = dimensions.x / 2.0f + scaledScreenDimensions.x / 2.0f;
		const float MIN_DISTANCE_Y = dimensions.y / 2.0f + scaledScreenDimensions.y / 2.0f;

		// Center position of the parameters
		vec2 centerPos = position + dimensions / 2.0f;
		// Center position of the camera
		vec2 centerCameraPos = m_position;
		// Vector from the input to the camera
		vec2 distVec = centerPos - centerCameraPos;

		// Get the depth of the collision
		float xDepth = MIN_DISTANCE_X - abs(distVec.x);
		float yDepth = MIN_DISTANCE_Y - abs(distVec.y);

		// If both the depths are > 0, then we collided
		if (xDepth > 0 && yDepth > 0) {
			// There was a collision
			return true;
		}
		return false;
	}

	void Camera2D::update()
	{
		if (m_needsUpdate)
		{
			vec3 translate(-m_position.x + m_screenWidth / 2, -m_position.y + m_screenHeight / 2, 0.0f);
			m_camMatrix = glm::translate(m_orthoMatrix, translate);

			vec3 scale(m_scale, m_scale, 0.0f);
			m_camMatrix = glm::scale(mat4(1.0f), scale) * m_camMatrix;

			m_needsUpdate = false;
		}
	}

}
