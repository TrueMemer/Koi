#include <Koi/Errors.h>

#include <cstdlib>
#include <iostream>

namespace Koi
{

	void FATAL(std::string a) {
		std::cout << "FATAL: " << a << "!" << std::endl;
		int b;
		std::cin >> b;
#ifdef KOI_SDL
		SDL_Quit();
#endif

#ifdef KOI_GLFW
		glfwTerminate();
#endif
		exit(1337);
	}

}