#include <Koi/SpriteBatch.h>

#include <algorithm>

using namespace glm;

namespace Koi {

	Glyph::Glyph() 
	{

	}

	Glyph::Glyph(const vec4 & rect, const vec4 & UVRect, GLuint _texture, const ColorRGBA8 & color, float _depth)
		: texture(_texture), depth(_depth)
	{
		topLeft.color = color;
		topLeft.setPosition(rect.x, rect.y + rect.w);
		topLeft.setUV(UVRect.x, UVRect.y + UVRect.w);

		bottomRight.color = color;
		bottomRight.setPosition(rect.x + rect.z, rect.y);
		bottomRight.setUV(UVRect.x + UVRect.z, UVRect.y);

		bottomLeft.color = color;
		bottomLeft.setPosition(rect.x, rect.y);
		bottomLeft.setUV(UVRect.x, UVRect.y);

		topRight.color = color;
		topRight.setPosition(rect.x + rect.z, rect.y + rect.w);
		topRight.setUV(UVRect.x + UVRect.z, UVRect.y + UVRect.w);
	}

	Glyph::Glyph(const vec4 & rect, const vec4 & UVRect, GLuint _texture, const ColorRGBA8 & color, float _depth, float angle)
		: texture(_texture), depth(_depth)
	{

		vec2 halfDims(rect.z / 2.f, rect.w / 2.f);

		vec2 tl(-halfDims.x, halfDims.y);
		vec2 bl(-halfDims.x, -halfDims.y);
		vec2 br(halfDims.x, -halfDims.y);
		vec2 tr(halfDims.x, halfDims.y);

		tl = rotate(tl, angle) + halfDims;
		bl = rotate(bl, angle) + halfDims;
		br = rotate(br, angle) + halfDims;
		tr = rotate(tr, angle) + halfDims;

		topLeft.color = color;
		topLeft.setPosition(rect.x + tl.x, rect.y + tl.y);
		topLeft.setUV(UVRect.x, UVRect.y + UVRect.w);

		bottomRight.color = color;
		bottomRight.setPosition(rect.x + br.x, rect.y + br.y);
		bottomRight.setUV(UVRect.x + UVRect.z, UVRect.y);

		bottomLeft.color = color;
		bottomLeft.setPosition(rect.x + bl.x, rect.y + bl.y);
		bottomLeft.setUV(UVRect.x, UVRect.y);

		topRight.color = color;
		topRight.setPosition(rect.x + tr.x, rect.y + tr.y);
		topRight.setUV(UVRect.x + UVRect.z, UVRect.y + UVRect.w);
	}

	vec2 Glyph::rotate(vec2 pos, float angle)
	{
		vec2 v;
		v.x = pos.x * cos(angle) - pos.y * sin(angle);
		v.y = pos.x * sin(angle) + pos.y * cos(angle);
		return v;
	}

	SpriteBatch::SpriteBatch()
	{
	}


	SpriteBatch::~SpriteBatch()
	{
	}

	void SpriteBatch::init()
	{
		createVertexArray();
	}

	void SpriteBatch::begin(GlyphSortType sortType)
	{
		m_sortType = sortType;
		m_batches.clear();
		m_glyphs.clear();
	}

	void SpriteBatch::end()
	{
		m_glyphPtrs.resize(m_glyphs.size());
		for (int i = 0; i < m_glyphs.size(); i++)
		{
			m_glyphPtrs[i] = &m_glyphs[i];
		}
		sortGlyphs();
		createRenderBatches();
	}

	void SpriteBatch::draw(const vec4& rect, const vec4& UVRect, GLuint texture, const ColorRGBA8& color, float depth)
	{
		m_glyphs.emplace_back(rect, UVRect, texture, color, depth);
	}

	void SpriteBatch::draw(const vec4& rect, const vec4& UVRect, GLuint texture, const ColorRGBA8& color, float depth, float angle)
	{
		m_glyphs.emplace_back(rect, UVRect, texture, color, depth, angle);
	}
	void SpriteBatch::draw(const vec4& rect, const vec4& UVRect, GLuint texture, const ColorRGBA8& color, float depth, const vec2 dir)
	{
		const vec2 right(1.f, 1.f);
		float angle = acos(dot(right, dir));
		if (dir.y < 0.f)
		{
			angle = -angle;
		}
		m_glyphs.emplace_back(rect, UVRect, texture, color, depth, angle);
	}

	void SpriteBatch::renderBatch()
	{
		glBindVertexArray(m_vao);

		for (int i = 0; i < m_batches.size(); i++) {
			glBindTexture(GL_TEXTURE_2D, m_batches[i].texture);
			glDrawArrays(GL_TRIANGLES, m_batches[i].offset, m_batches[i].numVertices);
		}

		glBindVertexArray(0);
	}

	void SpriteBatch::createRenderBatches()
	{
		std::vector<Vertex> vertices;
		vertices.resize(m_glyphPtrs.size() * 6);

		if (m_glyphPtrs.empty())
		{
			return;
		}


		// Current vertex
		int cv = 0;

		int offset = 0;
		m_batches.emplace_back(0, 6, m_glyphPtrs[0]->texture);

		vertices[cv++] = m_glyphPtrs[0]->topLeft;
		vertices[cv++] = m_glyphPtrs[0]->bottomLeft;
		vertices[cv++] = m_glyphPtrs[0]->bottomRight;
		vertices[cv++] = m_glyphPtrs[0]->bottomRight;
		vertices[cv++] = m_glyphPtrs[0]->topRight;
		vertices[cv++] = m_glyphPtrs[0]->topLeft;

		offset += 6;

		for (int cg = 1; cg < m_glyphPtrs.size(); cg++) {
			if (m_glyphPtrs[cg]->texture != m_glyphPtrs[cg - 1]->texture) {
				m_batches.emplace_back(offset, 6, m_glyphPtrs[cg]->texture);
			}
			else 
			{
				m_batches.back().numVertices += 6;
			}
			vertices[cv++] = m_glyphPtrs[cg]->topLeft;
			vertices[cv++] = m_glyphPtrs[cg]->bottomLeft;
			vertices[cv++] = m_glyphPtrs[cg]->bottomRight;
			vertices[cv++] = m_glyphPtrs[cg]->bottomRight;
			vertices[cv++] = m_glyphPtrs[cg]->topRight;
			vertices[cv++] = m_glyphPtrs[cg]->topLeft;
			offset += 6;
		}

		glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), nullptr, GL_STREAM_DRAW);
		glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size() * sizeof(Vertex), vertices.data());

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void SpriteBatch::createVertexArray()
	{
		if (m_vao == 0) 
		{
			glGenVertexArrays(1, &m_vao);
		}
		glBindVertexArray(m_vao);

		if (m_vbo == 0) 
		{
			glGenBuffers(1, &m_vbo);
		}
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

		// We are sending is just position
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		// Set up position attrib pointer
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, position));

		// Color attrib pointer
		glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), (void *)offsetof(Vertex, color));

		// UV coordinates attrib pointer
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, uv));

		glBindVertexArray(0);
	}

	void SpriteBatch::sortGlyphs() 
	{
		switch (m_sortType) {
		case GlyphSortType::BACK_TO_FRONT:
			std::stable_sort(m_glyphPtrs.begin(), m_glyphPtrs.end(), compareFrontToBack);
			break;
		case GlyphSortType::FRONT_TO_BACK:
			std::stable_sort(m_glyphPtrs.begin(), m_glyphPtrs.end(), compareBackToFront);
			break;
		case GlyphSortType::TEXTURE:
			std::stable_sort(m_glyphPtrs.begin(), m_glyphPtrs.end(), compareTexture);
			break;
		default:
			break;
		}
	}


	bool SpriteBatch::compareFrontToBack(Glyph *a, Glyph *b)
	{
		return (a->depth < b->depth);
	}
	bool SpriteBatch::compareBackToFront(Glyph *a, Glyph *b)
	{
		return (a->depth > b->depth);
	}
	bool SpriteBatch::compareTexture(Glyph *a, Glyph *b)
	{
		return (a->texture < b->texture);
	}

}
