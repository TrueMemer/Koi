#include <Koi/GLSL.h>
#include <Koi/Errors.h>

#include <fstream>
#include <vector>

namespace Koi
{

	GLSL::GLSL()
	{
	}


	GLSL::~GLSL()
	{
	}

	void GLSL::compile(const std::string& path, GLuint id)
	{
		//Vertex and fragment shaders are successfully compiled.
		//Now time to link them together into a program.
		//Get a program object.
		m_id = glCreateProgram();

		std::ifstream shaderFile(path);
		if (shaderFile.fail())
		{
			perror(path.c_str());
			FATAL("Can't load shader file \"" + path + "\" !\n");
		}

		std::string content = "";
		std::string line;

		// Parse vertex file to a single string
		while (std::getline(shaderFile, line)) { content += line + "\n"; }

		// Close file stream
		shaderFile.close();

		// What the fuck
		const char *ptr = content.c_str();

		// Set shader sources
		glShaderSource(id, 1, &ptr, nullptr);

		// Compile vertex shader
		glCompileShader(id);

		GLint success = 0;
		glGetShaderiv(id, GL_COMPILE_STATUS, &success);

		if (success == GL_FALSE)
		{
			GLint maxLength = 0;
			glGetShaderiv(id, GL_INFO_LOG_LENGTH, &maxLength);

			// The maxLength includes the NULL character
			std::vector<char> errorLog(maxLength);
			glGetShaderInfoLog(id, maxLength, &maxLength, &errorLog[0]);

			// Provide the infolog in whatever manor you deem best.
			// Exit with failure.
			glDeleteShader(id); // Don't leak the shader.

			printf("%s\n", &(errorLog[0]));
			FATAL("Shader \"" + path + "\" is failed to compile!\n");

			return;
		}
	}

	void GLSL::compileShaders(const std::string& vertex, const std::string& frag)
	{
		// Create vertex shader
		m_vertexID = glCreateShader(GL_VERTEX_SHADER);
		if (!m_vertexID) { FATAL("Can't create vertex shader!\n"); }

		// Create fragment shader
		m_fragmentID = glCreateShader(GL_FRAGMENT_SHADER);
		if (!m_fragmentID) { FATAL("Can't create fragment shader!\n"); }

		// Compile shader files
		compile(vertex, m_vertexID);
		compile(frag, m_fragmentID);
	}

	void GLSL::linkShaders()
	{
		//Attach our shaders to our program
		glAttachShader(m_id, m_vertexID);
		glAttachShader(m_id, m_fragmentID);

		//Link our program
		glLinkProgram(m_id);

		//Note the different functions here: glGetProgram* instead of glGetShader*.
		GLint isLinked = 0;
		glGetProgramiv(m_id, GL_LINK_STATUS, (int *)&isLinked);
		if (isLinked == GL_FALSE)
		{
			GLint maxLength = 0;
			glGetProgramiv(m_id, GL_INFO_LOG_LENGTH, &maxLength);

			//The maxLength includes the NULL character
			std::vector<char> infoLog(maxLength);
			glGetProgramInfoLog(m_id, maxLength, &maxLength, &infoLog[0]);

			//We don't need the program anymore.
			glDeleteProgram(m_id);
			//Don't leak shaders either.
			glDeleteShader(m_vertexID);
			glDeleteShader(m_fragmentID);

			//Use the infoLog as you see fit.

			printf("%s\n", &(infoLog[0]));
			FATAL("Can't link shaders!\n");
		}

		//Always detach shaders after a successful link.
		glDetachShader(m_id, m_vertexID);
		glDetachShader(m_id, m_fragmentID);
		glDeleteShader(m_vertexID);
		glDeleteShader(m_fragmentID);
	}

	void GLSL::addAttribute(const std::string& attrib)
	{
		glBindAttribLocation(m_id, attr++, attrib.c_str());
	}

	void GLSL::use()
	{
		glUseProgram(m_id);
		for (int i = 0; i < attr; i++)
		{
			glEnableVertexAttribArray(i);
		}
	}

	void GLSL::unuse()
	{
		glUseProgram(0);
		for (int i = 0; i < attr; i++)
		{
			glDisableVertexAttribArray(i);
		}
	}

	GLint GLSL::getUniformLocation(const std::string& uniform)
	{
		GLint location = glGetUniformLocation(m_id, uniform.c_str());
		if (location == GL_INVALID_INDEX)
		{
			FATAL("Uniform \"" + uniform + "\" is not found in shader!\n");
		}

		return location;
	}

}