#include <Koi/InputManager.h>

namespace Koi {

	InputManager::InputManager()
	{
	}


	InputManager::~InputManager()
	{
	}

	void InputManager::keypress(unsigned int key)
	{
		m_keymap[key] = true;
	}

	void InputManager::release(unsigned int key)
	{
		m_keymap[key] = false;
	}

	bool InputManager::isKeyDown(unsigned int key)
	{
		auto it = m_keymap.find(key);
		if (it != m_keymap.end())
		{
			return m_keymap[key];
		}
		return false;
	}

	bool InputManager::wasKeyDown(unsigned int key)
	{
		auto it = m_lastKeymap.find(key);
		if (it != m_lastKeymap.end())
		{
			return m_lastKeymap[key];
		}
		return false;
	}

	bool InputManager::isKeyPressed(unsigned int key)
	{
		return (isKeyDown(key) && wasKeyDown(key)) ? true : false;
	}

	void InputManager::update()
	{
		for (auto &it : m_keymap)
		{
			m_lastKeymap[it.first] = it.second;
		}
	}

	void InputManager::setMouseCoords(float x, float y)
	{
		m_mouseCoords.x = x;
		m_mouseCoords.y = y;
	}

}