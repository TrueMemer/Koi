#include <Koi/IOManager.h>

#include <fstream>

namespace Koi
{

	bool IOManager::readFileToBuffer(std::string& path, std::vector<unsigned char>& buf)
	{
		std::ifstream file(path, std::ios::binary);
		if (file.fail()) {
			perror(path.c_str());
			return false;
		}

		file.seekg(0, std::ios::end);

		// Get file size
		int size = file.tellg();

		file.seekg(0, std::ios::beg);

		// Reduce the file size by header
		size -= file.tellg();

		buf.resize(size);

		file.read((char*)&(buf[0]), size);
		file.close();

		return true;
	}

}
