#include <Koi/ResourceManager.h>

namespace Koi
{

	TextureCache ResourceManager::m_textureCache;

	GLTexture ResourceManager::getTexture(std::string path) { return m_textureCache.getTexture(path); }

}