#include <Koi/Sprite.h>

#include <Koi/Vertex.h>
#include <Koi/ResourceManager.h>

#include <cstddef>

namespace Koi
{

	Sprite::Sprite()
	{
		m_vbo = 0;
	}


	Sprite::~Sprite()
	{
		if (m_vbo) {
			glDeleteBuffers(1, &m_vbo);
		}
	}

	void Sprite::init(float x, float y, float width, float height, std::string texturePath)
	{
		m_x = x;
		m_y = y;
		m_width = width;
		m_height = height;

		m_texture = ResourceManager::getTexture(texturePath);

		// If VBO is not generated
		if (!m_vbo)
		{
			// Generate buffer and assign id to m_vbo variable
			glGenBuffers(1, &m_vbo);
		}

		Vertex vertexData[6];

		// First triangle
		vertexData[0].setPosition(x + width, y + width);
		vertexData[0].setUV(1.0f, 1.0f);

		vertexData[1].setPosition(x, y + height);
		vertexData[1].setUV(0.f, 1.0f);

		vertexData[2].setPosition(x, y);
		vertexData[2].setUV(0.f, 0.f);

		// Second triangle
		vertexData[3].setPosition(x, y);
		vertexData[3].setUV(0.f, 0.f);

		vertexData[4].setPosition(x + width, y);
		vertexData[4].setUV(1.0f, 0.f);

		vertexData[5].setPosition(x + width, y + height);
		vertexData[5].setUV(1.0f, 1.0f);

		for (int i = 0; i < 6; i++) {
			vertexData[i].setColor(255, 0, 255, 255);
		}

		vertexData[1].setColor(0, 0, 255, 255);
		vertexData[4].setColor(0, 255, 0, 255);

		// Bind buffer
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

		// Upload buffer data
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);

		// Unbind buffer
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void Sprite::draw()
	{
		// Bind texture
		glBindTexture(GL_TEXTURE_2D, m_texture.id);

		// Bind buffer
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo);


		// Draw
		glDrawArrays(GL_TRIANGLES, 0, 6);

		// Disable attrib
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);

		// Unbind buffer
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

}