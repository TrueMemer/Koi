#include <Koi/TextureCache.h>
#include <Koi/ImageLoader.h>

#include <iostream>

namespace Koi
{

	TextureCache::TextureCache()
	{
	}


	TextureCache::~TextureCache()
	{
	}

	GLTexture TextureCache::getTexture(std::string path)
	{
		// Check is texture is in map
		auto it = m_cache.find(path);

		// If not in the map then try to load it
		if (it == m_cache.end())
		{
			// Load texture
			GLTexture n = ImageLoader::loadPNG(path);

			// Insert texture
			m_cache.insert(std::make_pair(path, n));

			std::cout << "TC: Loaded texture \"" << path << "\"" << std::endl;

			// Return new texture
			return n;
		}

		std::cout << "TC: Loaded cached texture \"" << path << "\"" << std::endl;

		// Return texture
		return it->second;
	}

}