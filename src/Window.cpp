#include <Koi/Window.h>

#include <Koi/Errors.h>

namespace Koi
{

	Window::Window()
	{
	}


	Window::~Window()
	{
		glfwDestroyWindow(m_window);
	}

	void Window::update()
	{
		// Flush screen
		glfwSwapBuffers(m_window);

		// Pool events
		glfwPollEvents();

		glClearColor(m_backgroundColor.r,m_backgroundColor.g,m_backgroundColor.b,m_backgroundColor.a);
		glClear(GL_COLOR_BUFFER_BIT);
	}

	int Window::create(std::string title, int width, int height, unsigned int windowFlags)
	{
		// TODO: Implement flags for GLFW
		/*
		// Parse flags
		Uint32 flags = SDL_WINDOW_OPENGL;

		if (windowFlags & INVISIBLE)
		{
			flags |= SDL_WINDOW_HIDDEN;
		}

		if (windowFlags & FULLSCREEN)
		{
			flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
		}

		if (windowFlags & BORDERLESS)
		{
			flags |= SDL_WINDOW_BORDERLESS;
		}

		// Create window with m_width width and m_height height with OpenGL flag
		m_window = SDL_CreateWindow(title.c_str(),
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			width, height,
			flags);
			*/
		m_window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);

		if (m_window == nullptr) { FATAL("Can't create window!\n"); }
		
		// Get the ACTUAL screen width and height
		glfwGetFramebufferSize(m_window, &m_width, &m_height);

		// OpenGL Context
		glfwMakeContextCurrent(m_window);

		// GLEW initialization
		GLenum _g = glewInit();
		if (_g != GLEW_OK) { FATAL("Can't initialize GLEW!\n"); }
		glewExperimental = GL_TRUE;

		// Print OpenGL version
		std::printf("*** OpenGL Version %s ***\n", glGetString(GL_VERSION));

		// Create viewpoint
		glViewport(0, 0, m_width, m_height);

		// Default black color
		glClearColor(1.f, 1.f, 1.f, 1.0);

		// Enable V-Sync
		glfwSwapInterval(1);

		// Enable alpha blending
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		return 0;
	}

	void Window::setBackgroundColor(ColorRGBA8 color)
	{
		m_backgroundColor = color;
	}

	GLFWwindow *Window::getGLFWWindow()
	{
		return m_window;
	}

}
