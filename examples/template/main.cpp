#include <Koi.h>
#include <Koi/Window.h>
#include <Koi/InputManager.h>
#include <Koi/Timing.h>

#include <iostream>

int main() {
    Koi::init();

    Koi::Window win;
    Koi::InputManager input;
    Koi::FPSLimiter fps;
    win.create("Template", 800, 640, 0);
    win.setBackgroundColor(Koi::ColorRGBA8(1,1,0,1));

    fps.init(60.f);

    bool loop = true;
    while (loop == true) {

        fps.start();


        float _fps = fps.limit();

        std::cout << _fps << std::endl;

        win.update();
    }

    return 0;
}
